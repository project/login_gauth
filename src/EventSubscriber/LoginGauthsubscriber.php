<?php

namespace Drupal\login_gauth\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

/**
 * Class LoginGauthsubscriber.
 */
class LoginGauthsubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\TempStore\PrivateTempStoreFactory definition.
   *
   * @var Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStore;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var currentUser
   */
  protected $currentUser;

  /**
   * Constructs a new LoginGauthsubscriber object.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, PrivateTempStoreFactory $tempStore,  AccountProxy $currentUser) {
    $this->loggerFactory = $logger_factory;
    $this->tempStore = $tempStore->get('login_gauth');
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['kernel.request'] = ['onRequest'];

    return $events;
  }

  /**
   * This method is called when the kernel.request is dispatched.
   *
   * @param \Symfony\Component\EventDispatcher\Event $event
   *   The dispatched event.
   */
  public function onRequest(Event $event) {
    $path = $event->getRequest()->getRequestUri();
    if($this->allowToScanCode($path)) {
      return;
    }
    if(!$this->tempStore->get('user_' . $this->currentUser->id()) && $this->currentUser->isAuthenticated() && !$this->isValidationPath($path)) {
      $redirect = new RedirectResponse(Url::fromUserInput('/login_gauth/form/login_gauth')
      ->toString());
      $redirect->send();
    }
  }

  /**
   * Check if path is valid path.
   *
   * @param string $path
   *   A valid path.
   *
   * @return bool
   */
  private function isValidationPath($path) {
    if($path === '/login_gauth/form/login_gauth') {
      return TRUE;
    }
    return FALSE;    
  }

  /**
   * Check if the page is user page.
   *
   * @param string $path
   *   A valid path.
   *
   * @return bool
   */
  private function isUserPage($url_object) {
      if(strpos('entity.user.edit_form', $url_object->getRouteName()) !== FALSE || $url_object->getRouteName() === 'entity.user.canonical') {
        return TRUE;
      }
    return FALSE;
  }

  /**
   * Check path to allow user, if rescaning of QR code is allowed.
   *
   * @param string $path
   *   A valid path.
   *
   * @return bool
   */
  private function allowToScanCode($path) {
    $url_object = \Drupal::service('path.validator')->getUrlIfValid($path);
    if ($url_object) {
      if($this->isUserPage($url_object) 
        || $this->isAdmin()
        || $url_object->getRouteName() === 'login_gauth.gauth_login_qr_code_generator_controller_get'
        || $url_object->getRouteName() === 'login_gauth.gauth_login_qr_code_generator_controller_get_qr_code'
        || $this->useRecoveryCodes($url_object)
        || $url_object->getRouteName() === 'user.page') {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Check if user is admin.
   *
   * @return bool
   */
  private function isAdmin() {
    return in_array('administrator', $this->currentUser->getRoles()) ? TRUE : FALSE;
  }

  /**
   * 
   * @param type $url_object
   */
  private function useRecoveryCodes($url_object) {
    if($url_object->getRouteName() === 'login_gauth.recovery_codes_form') {
      return TRUE;
    }
    return FALSE;
  }
}
