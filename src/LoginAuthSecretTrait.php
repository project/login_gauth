<?php

namespace Drupal\login_gauth;

use Drupal\user\UserDataInterface;

/**
 * Provides methods to save login_gauth user settings.
 */
trait LoginAuthSecretTrait {

  /**
   * Store user specific information.
   *
   * @param string $module
   *   The name of the module the data is associated with.
   * @param array $data
   *   The value to store. Non-scalar values are serialized automatically.
   * @param int $uid
   *   The user id.
   * @param \Drupal\user\UserDataInterface $user_data
   *   User data object to store user specific information.
   */
  protected function setUserData($module, array $data, $uid, UserDataInterface $user_data) {
    $user_data->set(
      $module,
      $uid,
      key($data),
      current($data)
    );
  }

  /**
   * Returns data stored for the current validated user account.
   *
   * @param string $module
   *   The name of the module the data is associated with.
   * @param string $key
   *   The name of the data key.
   * @param int $uid
   *   The user id.
   * @param \Drupal\user\UserDataInterface $user_data
   *   User data object to store user specific information.
   *
   * @return mixed|array
   *   The stored value is returned, or NULL if no value was found.
   */
  protected function getUserData($module, $key, $uid, UserDataInterface $user_data) {
    $result = $user_data->get(
      $module,
      $uid,
      $key
    );

    return $result;
  }

  /**
   * Deletes data stored for the current validated user account.
   *
   * @param string $module
   *   The name of the module the data is associated with.
   * @param string $key
   *   The name of the data key.
   * @param int $uid
   *   The user id.
   * @param \Drupal\user\UserDataInterface $user_data
   *   User data object to store user specific information.
   */
  protected function deleteUserData($module, $key, $uid, UserDataInterface $user_data) {
    $user_data->delete(
      $module,
      $uid,
      $key
    );
  }

}
