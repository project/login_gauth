<?php

namespace Drupal\login_gauth\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\login_gauth\LoginAuthSecretTrait;
use Drupal\login_gauth\Response\QRImageResponse;
use Drupal\user\UserDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use PragmaRX\Google2FA\Google2FA;
use Drupal\Core\Session\AccountProxy;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;

/**
 * Class LoginQrCodeGeneratorController.
 */
class LoginQrCodeGeneratorController extends ControllerBase {

  use LoginAuthSecretTrait;

  // Qr code image size.
  const SIZE = 400;
  // Secret key length.
  const SECRET = 16;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Drupal\user\UserDataInterface; definition.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Drupal\Core\PageCache\ResponsePolicy\KillSwitch; definition.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $pageKillCache;

  /**
   * Drupal\Core\PageCache\ResponsePolicy\KillSwitch; definition.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $loggerFactory;

  /**
   * Constructs a new RoleLoginQrCodeGeneratorController object.
   */
  public function __construct(RequestStack $request_stack, AccountProxy $currentUser, UserDataInterface $userData, KillSwitch $pageKillCache) {
    $this->requestStack = $request_stack;
    $this->currentUser = $currentUser;
    $this->userData = $userData;
    $this->pageKillCache = $pageKillCache;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('current_user'),
      $container->get('user.data'),
      $container->get('page_cache_kill_switch'),
      $container->get('logger.factory')
    );
  }

  /**
   * Generate Qr code.
   *
   * @return \Drupal\login_gauth\Response\QRImageResponse
   *   It will return QRImageResponse.
   *
   * @throws \PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException
   *   It will throw In case of failure.
   * @throws \PragmaRX\Google2FA\Exceptions\InvalidCharactersException
   *   It will throw In case of failure.
   */
  public function get($userId = NULL) {
    try {
      $this->pageKillCache->trigger();
      $google2fa = new Google2FA();
      $secret = $google2fa->generateSecretKey(self::SECRET);
      $this->setUserData('login_gauth', ['secret' => $secret], $this->currentUser->id(), $this->userData);
      if ($this->currentUser->isAuthenticated()) {
        $inlineUrl = $google2fa->getQRCodeUrl(
          $this->requestStack->getCurrentRequest()->getHttpHost(), $this->currentUser->getEmail(), $secret
        );
        $writer = $this->getWriter();
        return new QRImageResponse($writer->writeString($inlineUrl));
      }
    }
    catch (\Exception $exception) {
      $this->loggerFactory->get('login_gauth')->error($exception->getMessage());
    }
  }

  /**
   * Get Writer Object.
   *
   * @return BaconQrCode\Writer
   *   It Will return Writer Object.
   */
  private function getWriter() {
    return new Writer($this->getImageRenderer());
  }

  /**
   * Get ImageRenderer Object.
   *
   * @return BaconQrCode\Renderer\ImageRenderer
   *   It Will return ImageRenderer Object.
   */
  private function getImageRenderer() {
    return new ImageRenderer(
      $this->getRendererStyle(),
      $this->getImageMagicBackend()
    );
  }

  /**
   * Get ImageMagicBackend Object.
   *
   * @return BaconQrCode\Renderer\Image\ImagickImageBackEnd
   *   It Will return ImagickImageBackEnd Object.
   */
  private function getImageMagicBackend() {
    return new ImagickImageBackEnd();
  }

  /**
   * Get Renderer Object.
   *
   * @return BaconQrCode\Renderer\RendererStyle\RendererStyle
   *   It Will return RendererStyle Object.
   */
  private function getRendererStyle() {
    return new RendererStyle(self::SIZE);
  }

}
