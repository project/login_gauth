<?php

namespace Drupal\login_gauth\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\login_gauth\LoginAuthSecretTrait;
use Drupal\user\UserDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use PragmaRX\Recovery\Recovery;
use Drupal\Core\Logger\LoggerChannelFactory;

/**
 * Class LoginQrCodeDecoratorController.
 */
class LoginQrCodeDecoratorController extends ControllerBase {

  use LoginAuthSecretTrait;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Drupal\user\UserDataInterface; definition.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Drupal\Core\PageCache\ResponsePolicy\KillSwitch; definition.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $pageKillCache;

  /**
   * Constructs a new FaceLoginQrCodeGeneratorController object.
   */
  public function __construct(RequestStack $request_stack, AccountProxy $currentUser, UserDataInterface $userData, KillSwitch $pageKillCache) {
    $this->requestStack = $request_stack;
    $this->currentUser = $currentUser;
    $this->userData = $userData;
    $this->pageKillCache = $pageKillCache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'), $container->get('current_user'), $container->get('user.data'), $container->get('page_cache_kill_switch')
    );
  }

  /**
   * Decorator for QR code.
   *
   * @return array
   *   Qr code Image.
   */
  public function getQrCode() {
    if ($this->getUserData('login_gauth', 'secret', $this->currentUser->id(), $this->userData)) {
      if ($this->currentUser->hasPermission('administer delete qrcode entries')) {
        $this->deleteUserData('login_gauth', 'secret', $this->currentUser->id(), $this->userData);
        $url = Url::fromRoute('login_gauth.gauth_login_qr_code_generator_controller_get_qr_code', ['user' => $this->currentUser->id()], ['query' => ['regenerate' => TRUE]]);
        $link = [
          '#type' => 'link',
          '#url' => $url,
          '#title' => $this->t('Click here to generate rendered Qr code image to scan.'),
        ];
        return $link;
      }
      else {
        return [
          '#type' => 'markup',
          '#markup' => $this->t('Please contact with site administrator to regenerate code.'),
          '#cache' => [
            'max-age' => 0
          ],
        ];
      }
    }
    if ($this->checkIfRegenerateImage() || empty($this->getUserData('login_gauth', 'secret', $this->currentUser->id(), $this->userData))) {
      $qrCodeImage = [
        '#theme' => 'image',
        '#uri' => Url::fromRoute('login_gauth.gauth_login_qr_code_generator_controller_get', ['user' => $this->currentUser->id()])->toString(),
      ];
      $this->recovery = new Recovery();
      $recovery = $this->recovery->numeric()->setCount(6)->setChars(6)->toArray();
      $codesArray = [];
      if (!empty($recovery)) {
        $codesArray = array_map(function($code) {
          $codes = explode('-', $code);
          return $codes;
        }, $recovery);
      }
      $this->setUserData('login_gauth', ['secret_' . $this->currentUser->id() => $codesArray], $this->currentUser->id(), $this->userData);
      return [
        '#theme' => 'qr_code',
        '#qr_code' => $qrCodeImage,
        '#recovery' => $codesArray,
        '#cache' => [
          'max-age' => 0,
        ],
      ];
    }
  }

  /**
   * Check if renegerated image requested.
   *
   * @return mixed
   *   Will return true or null.
   */
  private function checkIfRegenerateImage() {
    return $this->requestStack->getCurrentRequest()->get('regenerate');
  }

}
