<?php

namespace Drupal\login_gauth\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\login_gauth\LoginAuthSecretTrait;

/**
 * Class RecoveryCodesForm.
 */
class RecoveryCodesForm extends FormBase {

  use LoginAuthSecretTrait;

  /**
   * Drupal\user\UserDataInterface definition.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\user\PrivateTempStoreFactory definition.
   *
   * @var Drupal\user\PrivateTempStoreFactory 
   */
  protected $tempStore;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->userData = $container->get('user.data');
    $instance->currentUser = $container->get('current_user');
    $instance->requestStack = $container->get('request_stack');
    $instance->tempStore = $container->get('tempstore.private');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'recovery_codes_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['recovery_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter Recovery code here.'),
      '#description' => $this->t('Enter Recovery code to bypass TFA'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    try {
      $account = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
      if (!empty($account)) {
        $codes = $this->getUserData('login_gauth', 'secret_' . $this->currentUser->id(), $this->currentUser->id(), $this->userData);
        if (empty($codes)) {
          $form_state->setErrorByName('recovery_code', $this->t('No Recovery Codes left.'));
        }
        foreach ($codes as $code) {
          foreach ($code as $nestedCode) {
            $codesArray[] = $nestedCode;
          }
        }
        if (!in_array($form_state->getValue('recovery_code'), $codesArray)) {
          $form_state->setErrorByName('recovery_code', $this->t('Wrong recovery code entered.'));
        }
      }
      else {
        $form_state->setErrorByName('recovery_code', $this->t('Something went Wrong'));
      }
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('recovery_code', $this->t('Error...'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $recoveryCode = $form_state->getValue('recovery_code');
    $codes = $this->getUserData('login_gauth', 'secret_' . $this->currentUser->id(), $this->currentUser->id(), $this->userData);
    foreach ($codes as $upperKey => $code) {
      foreach ($code as $key => $nestedCode) {
        if ($nestedCode === (string) $recoveryCode) {
          unset($codes[$upperKey][$key]);
        }
      }
    }
    $this->setUserData('login_gauth', ['secret_' . $this->currentUser->id() => $codes], $this->currentUser->id(), $this->userData);
    $this->tempStore->get('login_gauth')->set('user_' . $this->currentUser->id(), TRUE);
    // Redirect to user profile page.
    $form_state->setRedirect('user.page');
  }

}
