<?php

namespace Drupal\login_gauth\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\user\UserDataInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\login_gauth\LoginAuthSecretTrait;
use PragmaRX\Google2FA\Google2FA;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;

/**
 * Class LoginGauthForm.
 */
class LoginGauthForm extends FormBase {

  use LoginAuthSecretTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var currentUser
   */
  protected $currentUser;

  /**
   * Drupal\user\UserDataInterface definition.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\TempStore\PrivateTempStoreFactory definition.
   *
   * @var Drupal\Core\TempStore\PrivateTempStoreFactory 
   */
  protected $tempStore;

  /**
   * Constructs a new RoleLoginGAuthForm object.
   */
  public function __construct(
  EntityTypeManagerInterface $entity_type_manager,
  AccountProxy $currentUser,
  UserDataInterface $userData,
  ConfigFactoryInterface $configFactory,
  PrivateTempStoreFactory $tempStore
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->userStorage = $this->entityTypeManager->getStorage('user');
    $this->currentUser = $currentUser;
    $this->userData = $userData;
    $this->configFactory = $configFactory;
    $this->tempStore = $tempStore->get('login_gauth');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('user.data'),
      $container->get('config.factory'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'login_gauth_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Code'),
      '#description' => $this->t('Enter code to validate'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['test_ink'] = [
      '#type' => 'link',
      '#title' => $this->t('Use Recovery codes'),
      '#url' => Url::fromRoute('login_gauth.recovery_codes_form'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    $form['#validate'][] = '::validateGauth';
    $form['#cache']['max-age'] = 0;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateGauth(array &$form, FormStateInterface $form_state) {
    try {
      $account = $this->userStorage->load($this->currentUser->id());
      if (!empty($account)) {
        $google2fa = new Google2FA();
        $secret = $form_state->getValue('code');
        $key = $this->getUserData('login_gauth', 'secret', $account->id(), $this->userData);
        if ($google2fa->verifyKey($key, $secret) === FALSE) {
          $form_state->setErrorByName('code', $this->t('Error code not matching.' . $google2fa->getCurrentOtp($key)));
        }
      }
      else {
        $form_state->setErrorByName('code', $this->t('Something went Wrong'));
      }
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('code', $this->t('Errord code not matching.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function validateGauth2(array &$form, FormStateInterface $form_state) {
    try {
      $accounts = $this->userStorage->loadByProperties(['name' => $form_state->getValue('username')]);
      if (!empty($accounts)) {
        $account = reset($accounts);
        $google2fa = new Google2FA();
        $secret = $form_state->getValue('code');
        $key = $this->getUserData('role_gauth_login', 'secret', $account->id(), $this->userData);
        if ($google2fa->verifyKey($key, $secret) === FALSE) {
          $form_state->setErrorByName('code', $this->t('Error code not matching.'));
        }
      }
      else {
        $form_state->setErrorByName('username', $this->t('Username does not exist.'));
      }
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('code', $this->t('Error code not matching.'));
    }

  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Load the user account.
    $account = $this->userStorage->load($this->currentUser->id());
    $this->tempStore->set('user_' . $this->currentUser->id(), TRUE);
    // Redirect to user profile page.
    $form_state->setRedirect('user.page');
  }

}
