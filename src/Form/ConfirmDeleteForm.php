<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Drupal\login_gauth\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\login_gauth\LoginAuthSecretTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class ConfirmDeleteForm extends ConfirmFormBase {

  use LoginAuthSecretTrait;

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $id;

  /**
   * Drupal\user\UserDataInterface definition.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\user\PrivateTempStoreFactory definition.
   *
   * @var Drupal\user\PrivateTempStoreFactory 
   */
  protected $tempStore;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->userData = $container->get('user.data');
    $instance->currentUser = $container->get('current_user');
    $instance->requestStack = $container->get('request_stack');
    $instance->tempStore = $container->get('tempstore.private');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL) {
    $this->id = $id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if(in_array('administrator', $this->currentUser()->getRoles()) || (string) $this->id === (string) $this->currentUser()->id()) {
      $this->tempStore->get('login_gauth')->set('user_' . $this->id, TRUE);
      $this->deleteUserData('login_gauth', 'secret', $this->id, $this->userData);
      $this->deleteUserData('login_gauth', 'secret_' . $this->id, $this->id, $this->userData);
      $this->messenger()->addMessage($this->t('Recovery code QR code data deleted successfully for %id.', ['%id' => $this->id]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "confirm_delete_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('<front>');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Do you want to delete %id?', ['%id' => $this->id]);
  }

}