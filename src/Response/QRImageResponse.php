<?php

namespace Drupal\login_gauth\Response;

use Symfony\Component\HttpFoundation\Response;

/**
 * Response which is returned as the QR code.
 *
 * @package Drupal\face_login_gauth\Response
 */
class QRImageResponse extends Response {

  /**
   * String to be converted to QR Code.
   *
   * @var data
   */
  private $data;

  /**
   * Recourse with generated image.
   *
   * @var resource
   */
  protected $image;

  /**
   * {@inheritdoc}
   */
  public function __construct($content, $status = 200, $headers = []) {
    parent::__construct(NULL, $status, $headers);
    $this->data = $content;
  }

  /**
   * {@inheritdoc}
   */
  public function sendHeaders() {
    $this->headers->set('content-type', 'image/jpeg');
    return parent::sendHeaders();
  }

  /**
   * {@inheritdoc}
   */
  public function sendContent() {
    $this->generateQrCode($this->data);
  }

  /**
   * Function generate QR code for the string or URL.
   *
   * @param string $string
   *   String to generate Qr code against.
   */
  private function generateQrCode(string $string = NULL) {
    $im = imagecreatefromstring($string);
    ob_start();
    imagejpeg($im);
    imagedestroy($im);
  }

}
