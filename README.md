## Login with GAuth module for Drupal
### INTRODUCTION
login_gauth module for providing login along with google authenticator
for your Drupal site. Module provides login facilty to be more secure with
integration to google authentication.
### REQUIREMENTS

For Google Authenticator it needs:
````
composer require pragmarx/google2fa
````
For Qr Code generation:
````
composer require bacon/bacon-qr-code
````
For Recovery codes:

````
composer require pragmarx/recovery
````
Read more about the features and use of login_gauth at its Drupal.org project page at
https://drupal.org/project/login_gauth

### INSTALLATION

Login Gauth module can be installed like other Drupal modules by placing this directory
in the Drupal file system (for example, under modules/) and enabling on
the Drupal modules page.

